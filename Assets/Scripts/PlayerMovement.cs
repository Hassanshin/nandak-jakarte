﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net.Sockets;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float moveSpeed = 5f;
    private Vector3 forward, right;

    private Camera cam;
    private Rigidbody rb;

    private void Start()
    {
        cam = Camera.main;
        rb = GetComponent<Rigidbody>();

        forward = cam.transform.forward;
        forward.y = 0;
        forward = Vector3.Normalize(forward);
        right = Quaternion.Euler(new Vector3(0, 90, 0)) * forward;
    }

    private void FixedUpdate()
    {
    }

    public void MovePlayer(float hor, float ver)
    {
        Vector3 dir = new Vector3(hor, 0, ver);
        Vector3 rightMovement = right * moveSpeed * Time.deltaTime * hor;
        Vector3 upMovement = forward * moveSpeed * Time.deltaTime * ver;

        Vector3 heading = Vector3.Normalize(rightMovement + upMovement);

        if (heading != Vector3.zero)
            transform.forward = heading;

        rb.MovePosition(transform.position + (rightMovement + upMovement));
    }
}