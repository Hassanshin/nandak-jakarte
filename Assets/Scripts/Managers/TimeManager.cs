﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System;

public class TimeManager : MonoBehaviour
{
    public bool IsPaused;

    [SerializeField]
    private float gameEndsTime = 60f;

    [SerializeField]
    private float currentTime;

    public OnTimeTick OnSpawnTickCallback;

    public delegate void OnTimeTick();

    public OnTimeProgress OnTimeProgressCallback;

    public delegate void OnTimeProgress();

    public float SpawnTick = 5f;

    private float spawnTickTime;

    public static TimeManager Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Update()
    {
        if (IsPaused)
            return;

        if (currentTime > gameEndsTime)
        {
            gameOver();
            return;
        }

        currentTime += Time.deltaTime;
        string timeString = $"{ConvertIntegerToHours(currentTime)}";

        spawnTickTime += Time.deltaTime;

        OnTimeProgressCallback?.Invoke();

        if (spawnTickTime >= SpawnTick)
        {
            spawnTickTime -= SpawnTick;
            OnSpawnTickCallback?.Invoke();
        }
    }

    private void gameOver()
    {
        IsPaused = true;
    }

    private string ConvertIntegerToHours(float i)
    {
        int minutes = (int)i / 60;
        int seconds = (int)i % 60;

        return string.Format("{0:00}:{1:00}", minutes, seconds);
    }
}