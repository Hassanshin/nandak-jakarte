﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    [SerializeField]
    private int currentLevel;

    [Header("Components")]
    [SerializeField]
    private GameObject coinPrefab;

    [SerializeField]
    private List<Transform> coinSpawnPos = new List<Transform>();

    [SerializeField]
    private List<GameObject> spawnedCoins = new List<GameObject>();

    private TimeManager time;

    private void Start()
    {
        time = TimeManager.Instance;
        time.OnSpawnTickCallback += spawnCoin;
    }

    private void spawnCoin()
    {
        int i = Random.Range(0, coinSpawnPos.Count);
        GameObject _spawnedCoin = Instantiate(coinPrefab, coinSpawnPos[i].transform.position, Quaternion.identity);
        spawnedCoins.Add(_spawnedCoin);
    }

    public void RemoveCoin(GameObject _spawnedCoin)
    {
        spawnedCoins.Remove(_spawnedCoin);
    }
}