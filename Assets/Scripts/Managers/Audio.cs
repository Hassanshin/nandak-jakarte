﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Audio : MonoBehaviour
{
    public List<AudioClip> SfxClip;

    public AudioClip BgmMainMenu;
    public List<AudioClip> BgmLevel;

    [Header("components")]
    public AudioSource SfxSource;
    public AudioSource BgmSource;

    public AudioMixer Master;

    #region instancing

    public static Audio Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
        }
    }

    #endregion instancing

    public void PlaySfx(int index)
    {
        SfxSource.PlayOneShot(SfxClip[index]);
    }

    public void PlayBgmMainMenu()
    {
        BgmSource.clip = BgmMainMenu;
        BgmSource.Play();
    }

    public void PlayBgmLevel(int index)
    {
        BgmSource.clip = BgmLevel[index];
        BgmSource.Play();
    }

    public void SetSfxVolume(float volume)
    {
        var vol = (1 - Mathf.Sqrt(volume)) * -80f;
        Master.SetFloat("SFX", vol);
    }

    public void SetSfx(bool _state)
    {
        float volume;
        Master.GetFloat("SFX", out volume);
        Master.SetFloat("SFX", volume >= 0 ? -80 : 0);
    }

    public void SetBgmVolume(float volume)
    {
        var vol = (1 - Mathf.Sqrt(volume)) * -80f;
        Master.SetFloat("BGM", vol);
    }

    public void SetBgm(bool _state)
    {
        Master.SetFloat("BGM", _state ? 0 : -80);
    }
}