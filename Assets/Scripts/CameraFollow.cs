﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour
{
    [SerializeField]
    private float speed = 1;

    [SerializeField]
    private Transform followedObject;

    private void LateUpdate()
    {
        transform.position = Vector3.Lerp(transform.position, followedObject.position, speed * Time.deltaTime);
    }
}