﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : MonoBehaviour
{
    public static InputManager Instance;

    [SerializeField]
    private Joystick joystick;

    [SerializeField]
    private PlayerMovement player;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
        }
    }

    private void FixedUpdate()
    {
        player.MovePlayer(joystick.Horizontal, joystick.Vertical);
    }
}